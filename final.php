<?php

/*STV INC 2014 gtfs import script to import gtfs data from NJ transit and insert into the db
the data is in google gtfs specification for more details about the specifications 
visit GTFS info: https://developers.google.com/transit/gtfs/ and NJT Developers page: https://www.njtransit.com/developer
*/

//the zip file is in the current directory 
$file = 'rail_data.zip';
// Create connection to database server
include "dbinfo.inc.php";
try {
	$dbm = new PDO("mysql:dbname=$dbname;host=$dbhost",
		  $dbuser,
		  $dbpass);
} catch (PDOException $e) {
	print "DB Connection Failed: " . $e->getMessage() . "<br/>";
	die();
}

//get the absolute path to $file
$path = pathinfo(realpath($file),PATHINFO_DIRNAME); // need to give a different path for the file to open and extract within a temporary directory 

// Calculate hash of the zip file
$fileHash = sha1_file($file);

// perform query on gtfsImportHistory to see if an existing import with the
// same hash exists
$stmt = $dbm->prepare("SELECT hash from gtfsImportHistory where hash = :hash");
$stmt->bindParam(':hash', $fileHash);

if($stmt->execute()){
	if($row = $stmt->fetch()) { 
		print "File already imported - hash exists in database";
		exit();
	}
}

// If an existing import did not exist
$zip = new ZipArchive;
$res = $zip->open($file);


if($res == TRUE){
  $zip->extractTo('/home/gunarad/public_html/testing');
  $zip->close();
    
  echo $path;
  echo "$file extracted to $path \n";
  //$dbm->beginTransaction(); 

	// Insert (NULL, now(), calculated hash) into gtfsImportHistory
	$stmt = $dbm->prepare("INSERT INTO gtfsImportHistory (id,date,hash) VALUES(NULL, now(),:fileHash)");
	$stmt->bindParam(':fileHash',$fileHash);
	if($stmt->execute()) {
           print "inserted into gtfsImportHistory\n";
        }
        else {
           print "failed insert to gtfsImportHistory\n";
        }
	
	//get the most recent inserted row from MySQL (look at PDO documentation)
	$importRowId = $dbm->lastInsertId(); 
        print "Row id: $importRowId \n";
	//store it as $importRowId
	
	// open file with handler
	//$_inputfiles = ('agency.txt', 'stops.txt');
	
	$handle = @fopen("agency.txt","r") or die("could not open agency"); //read line by line
	$first_line = fgetcsv($handle,1000,",");//ignore the 1st line of the file 
      while (($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		//$agency_id = $file_open[1];
		$agency_name = $file_open[1];
		$agency_url = $file_open[2];
		$agency_timezone = $file_open[3];
		$agency_lang = $file_open[4];
		$agency_phone = $file_open[5];

		$stmt = $dbm->prepare("INSERT INTO agency (id,agency_id,agency_name,agency_url,agency_timezone,agency_lang,agency_phone,importRowId) VALUES (NULL,NULL, :agency_name, :agency_url, :agency_timezone, :agency_lang, :agency_phone, :importRowId)");
		//$stmt->bindParam(':agency_id', $agency_id,PDO::PARAM_STR);no need for this parameter 
		$stmt->bindParam(':agency_name', $agency_name,PDO::PARAM_STR);	
		$stmt->bindParam(':agency_url',$agency_url,PDO::PARAM_STR);
		$stmt->bindParam(':agency_timezone', $agency_timezone, PDO::PARAM_STR);
		$stmt->bindParam(':agency_lang', $agency_lang, PDO::PARAM_STR);	
		$stmt->bindParam(':agency_phone', $agency_phone, PDO::PARAM_STR);
		$stmt->bindParam(':importRowId', $importRowId);			
		
		if($stmt->execute()) {
			print "agency.txt inserted into db\n";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}
	}
	fclose($handle);
	print "handle close\n";	

	//open handler for calender_dates 	
	$handle = @fopen("calendar_dates.txt","r") or die ("could not open calender_dates\n");
	$first_line = fgetcsv($handle,1000,",");
	while(($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		$service_id = $file_open[0];
		$date = $file_open[1];
		$exception_type = $file_open[2];
		
		$stmt = $dbm->prepare("INSERT INTO calendar_dates (id,service_id,date,exception_type,importRowId) VALUES (NULL,:service_id,:date,:exception_type,:importRowId)");
		$stmt->bindParam(':service_id', $service_id,PDO::PARAM_STR);
		$stmt->bindParam(':date', $date,PDO::PARAM_STR);	
		$stmt->bindParam(':exception_type',$exception_type,PDO::PARAM_STR);
		$stmt->bindParam(':importRowId',$importRowId);
	
		if($stmt->execute()) {
			print "calender_dates.txt inserted into db";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}
	}
	fclose($handle);
		
	//inserting routes.txt into db
	$handle = @fopen("routes.txt","r") or die ("could not open routes\n");
	$first_line = fgetcsv($handle,1000,",");
	while (($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		$route_id = $file_open[0];
		$agency_id = $file_open[1];
		$route_short_name = $file_open[2];
		$route_long_name = $file_open[3];
		$route_type = $file_open[4];
		$route_url = $file_open[5];
		//$route_color = $file_open[6];
		
		$stmt = $dbm->prepare("INSERT INTO routes (id,route_id,agency_id,route_short_name,route_long_name,route_type,route_url,importRowId) VALUES (NULL,:route_id,:agency_id,:route_short_name,:route_long_name,:route_type,:route_url,:importRowId)");
		$stmt->bindParam(':route_id', $route_id,PDO::PARAM_STR);
		$stmt->bindParam(':agency_id', $agency_id,PDO::PARAM_STR);	
		$stmt->bindParam(':route_short_name',$route_short_name,PDO::PARAM_STR);
		$stmt->bindParam(':route_long_name', $route_long_name,PDO::PARAM_STR);
		$stmt->bindParam(':route_type', $route_type,PDO::PARAM_STR);	
		$stmt->bindParam(':route_url', $route_url,PDO::PARAM_STR);
		$stmt->bindParam(':importRowId', $importRowId);	
		
	print "upto here\n";
		if($stmt->execute()) {
			print "routes.txt inserted into db";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}
			
	}
	fclose($handle);
	/*
	shapes file is not use, it's only for mapping 
	$handle = @fopen("shapes.txt") or die ("could not open shapes");

	while ($file_open = fgetcsv($handle,1000,",") !- false){
		$shape_id = $file_open[0];
		$shape_pt_lat = $file_open[1];
		$shape_pt_lon = $file_open[2];
		$shape_pt_sequence = $file_open[3];
		$shape_dist_traveled = $file_open[4];
		
		$sql = mysql_query("INSERT INTO shapes (shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled)
			VALUES ('$shape_id','$shape_pt_lat','$shape_pt_lon','$shape_pt_sequence','$shape_dist_traveled')");
			
	}

	fclose($handle);
	*/
	/*
	//inserting stop times into db 
	$handle = @fopen("stop_times.txt","r") or die ("could not open stop_times");

	while (($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		$trip_id = $file_open[0];
		$arrival_time = $file_open[1];
		$departure_time = $file_open[2];
		$stop_id = $file_open[3];
		$stop_sequence = $file_open[4];
		$pickup_type = $file_open[5];
		$drop_off_type = $file_open[6];
		$shape_dist_traveled = $file_open[7];
		
		$stmt = $dbm->prepare("INSERT INTO stop_times (id,trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type,shape_dist_traveled,importRowId) VALUES (NULL,:trip_id,:arrival_time,:departure_time,:stop_id,:stop_sequence,:pickup_type,:drop_off_type,:shape_dist_traveled,:importRowId)");
		$stmt->bindParam(':trip_id', $trip_id,PDO::PARAM_STR);
		$stmt->bindParam(':arrival_time', $arrival_time,PDO::PARAM_STR);	
		$stmt->bindParam(':departure_time',$departure_time,PDO::PARAM_STR);
		$stmt->bindParam(':stop_id', $stop_id,PDO::PARAM_STR);
		$stmt->bindParam(':stop_sequence', $stop_sequence,PDO::PARAM_STR);	
		$stmt->bindParam(':pickup_type', $pickup_type,PDO::PARAM_STR);
		$stmt->bindParam(':drop_off_type',$drop_off_type,PDO::PARAM_STR);
		$stmt->bindParam(':importRowId', $importRowId);			
	print "upto here\n";
		if($stmt->execute()) {
			print "stop_times.txt inserted into db";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}
		
	}

	fclose($handle);
	*/
	
	//inserting stops.txt into the db
	$handle = @fopen("stops.txt","r") or die ("could not open stops");
	$first_line = fgetcsv($handle,1000,",");
	while (($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		$stop_id = $file_open[0];
		$stop_code = $file_open[1];
		$stop_name = $file_open[2];
		$stop_desc = $file_open[3];
		$stop_lat = $file_open[4];
		$stop_lon = $file_open[5];
		$zone_id = $file_open[6];
		
		$stmt = $dbm->prepare("INSERT INTO stops (id,stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,importRowId) VALUES (NULL,:stop_id,:stop_code,:stop_name,:stop_desc,:stop_lat,:stop_lon,:zone_id,:importRowId)");
		$stmt->bindParam(':stop_id', $stop_id,PDO::PARAM_STR);
		$stmt->bindParam(':stop_code', $stop_code,PDO::PARAM_STR);	
		$stmt->bindParam(':stop_name',$stop_name,PDO::PARAM_STR);
		$stmt->bindParam(':stop_desc', $stop_desc,PDO::PARAM_STR);
		$stmt->bindParam(':stop_lat', $stop_lat,PDO::PARAM_STR);	
		$stmt->bindParam(':stop_lon', $stop_lon,PDO::PARAM_STR);
		$stmt->bindParam(':zone_id',$zone_id,PDO::PARAM_STR);
		$stmt->bindParam(':importRowId', $importRowId);	
		
	print "upto here\n";
		if($stmt->execute()) {
			print "stops.txt inserted into db";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}		
	}

	fclose($handle);
	
	//inserting trips.txt into the db
	$handle = @fopen("trips.txt","r") or die ("could not open trips");
	$first_line = fgetcsv($handle,1000,",");
	while (($file_open = fgetcsv($handle,1000,",")) !== FALSE){
		$route_id = $file_open[0];
		$service_id = $file_open[1];
		$trip_id = $file_open[2];
		$trip_headsign = $file_open[3];
		$direction_id = $file_open[4];
		$block_id = $file_open[5];
		$shape_id = $file_open[6];
		
		$stmt = $dbm->prepare("INSERT INTO trips (id,route_id,service_id,trip_id,trip_headsign,direction_id,block_id,shape_id,importRowId) VALUES (NULL,:route_id,:service_id,:trip_id,:trip_headsign,:direction_id,:block_id,:shape_id,:importRowId)");
		$stmt->bindParam(':route_id', $route_id,PDO::PARAM_STR);
		$stmt->bindParam(':service_id', $service_id,PDO::PARAM_STR);	
		$stmt->bindParam(':trip_id',$trip_id,PDO::PARAM_STR);
		$stmt->bindParam(':trip_headsign', $trip_headsign,PDO::PARAM_STR);
		$stmt->bindParam(':direction_id', $direction_id,PDO::PARAM_STR);	
		$stmt->bindParam(':block_id', $block_id,PDO::PARAM_STR);
		$stmt->bindParam(':shape_id',$shape_id,PDO::PARAM_STR);
		$stmt->bindParam(':importRowId', $importRowId);			
	print "upto here\n";
		if($stmt->execute()) {
			print "trips.txt inserted into db";
		}
		else {
			print "Execute error.\n";
			//$dbm->rollback(); 
			exit();
		}				
	}
	fclose($handle);

	unlink($file);
	//$dbm->commit(); 
	exit();
}

else{
  echo "Couldn't open $file";
}
?>
