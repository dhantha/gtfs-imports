#!/usr/bin/perl

use strict;
use warnings;

my $animal = "camel";
my $answer = 42;

#print $animal;
#print "The animal is $animal\n";
#print "The square of answer is ", $answer * $answer, "\n";

my @animals = ("camel","llama","owl");
my @numbers = (23, 42, 69);
my @mixed = ("camel",42,1.23);

#print $animals[0];
#print $mixed[$#mixed];

#print @animals[0,1];

my @sorted = sort @animals;
my @backwards = reverse @numbers;

#print @sorted;
#print "\n";
#print @backwards;

#my %fruit_color = ("apple","red","banana","yellow");

my %fruit_color = (
	apple => "red",
	banana => "yellow",
);
#$fruit_color["apple"];

my @fruits = keys %fruit_color;
my @colors = values %fruit_color;

#print @fruits;
#print @colors;

foreach my $key (keys %fruit_color){
	print "The value of $key is $fruit_color{$key}\n";
}


